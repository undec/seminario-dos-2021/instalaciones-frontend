# Instalaciones necesarias para el curso



## Instalaciones Backend

-   [Java](https://www.oracle.com/java/technologies/downloads/)
-   [Maven](https://maven.apache.org/)
-   [MySQL](https://www.mysql.com/downloads/)
-   [Git](https://git-scm.com/downloads)
-   [IntelliJ](https://www.jetbrains.com/idea/download/?source=google&medium=cpc&campaign=9736964635&gclid=Cj0KCQjw1ouKBhC5ARIsAHXNMI9seyzMzCp8d3-d083n0POno22BfKB660SToQoSQsvIKMm8WQXi5WQaAmg_EALw_wcB#section=windows)
-   [Google Chrome](https://www.google.com/chrome/?brand=BNSD&gclid=Cj0KCQjw1ouKBhC5ARIsAHXNMI_2yM7cggC4aYZWxz_ReDIQcZG07wheX9cKa6goE0MFJ0v4ED_cLb4aAtVsEALw_wcB&gclsrc=aw.ds)
-   [Postman](https://www.postman.com/downloads/)

## Instalaciones Frontend

-   [Node](https://nodejs.org/es/)
-   [Angular cli](https://angular.io/cli)
-   [Ionic framework](https://ionicframework.com/docs/intro/cli)
-   [WebStorm](https://www.jetbrains.com/webstorm/promo/?source=google&medium=cpc&campaign=9641686239&gclid=Cj0KCQjw1ouKBhC5ARIsAHXNMI-aWPr02oGmzzbdzaK8yZk8Xhc1PIO7SsEgeN8u99yRhwb9Jy9Ag5UaAlY7EALw_wcB)
-   [Visual Studio Code](https://code.visualstudio.com/download)
